# tglib 
这是一个简易的、很可能会坑掉的、由php编写而成的 telegram bot api 函式库。  

使用方法：  
先创建好你的Bot，set好webhook  
然后下载或者 `git clone`  
然后在你的代码里键入
```
$input = json_decode(file_get_contents('php://input'),true);
$config = [
    "token" => 'the token of your bot'
];
include("./tglib.php");
```
然后就可以开始使用了

目前已经完成了
- 发送会话状态(sendChatAction);
- 发送消息
- 回复一个消息
- 下载外部图片
- 发送图片
- 发送视频