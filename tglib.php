<?php
function action($action){
    global $input, $config;
    return file_get_contents('https://api.telegram.org/bot'.$config['token'].'/sendChatAction?chat_id='.$input['message']['chat']['id'].'/&action='.$action);
}

function reply($content,$replyMessageID){
	global $input,$config;
	$data = array(
		"chat_id" => $input['message']['chat']['id'],
        "text" => $content,
        "reply_to_message_id" => $replyMessageID,
		"parse_mode" => 'Markdown'
	);
	$data_string = json_encode($data);
	$ch = curl_init('https://api.telegram.org/bot'.$config['token'].'/sendMessage');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string))
	);
	return curl_exec($ch);
}

function send($content){
    global $input, $config;
    $data = array(
        "chat_id" => $input['message']['chat']['id'],
        "text" => $content,
        "parse_mode" => 'markdown'
    );
    $data_string = json_encode($data);
    $ch = curl_init('https://api.telegram.org/bot'.$config["token"].'/sendMessage');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string)
    ));
    return curl_exec($ch);
    curl_close($ch);
}

function dlimg($url, $file = "", $timeout = 60){
    $file = empty($file) ? pathinfo($url, PATHINFO_BASENAME) : $file;
    $dir = pathinfo($file, PATHINFO_DIRNAME);
    !is_dir($dir) && @mkdir($dir, 0755, true);
    $url = str_replace(" ", "%20", $url);
    if (function_exists('curl_init')) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $temp = curl_exec($ch);
        if (@file_put_contents($file, $temp) && !curl_error($ch)) {
            return $file;
        }
        else {
            return false;
        }
    }
    else {
        $opts = array(
            "http" => array(
                "method" => "GET",
                "header" => "",
                "timeout" => $timeout
            )
        );
        $context = stream_context_create($opts);
        if (@copy($url, $file, $context)) {
      //$http_response_header
            return $file;
        }
        else {
            return false;
        }
    }
}

function sendPhoto($img,$caption){
    global $input, $config;
    $url = "https://api.telegram.org/bot".$config["token"]."/sendPhoto?";
    $post_fields = array(
        'chat_id' => $input["message"]["chat"]["id"],
        'photo' => new CURLFile(realpath("$img")),
        'caption' => "$caption"
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type:multipart/form-data"
    ));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    $output = curl_exec($ch);
}

function sendVideo($img){
    global $input, $config;
    $url = "https://api.telegram.org/bot".$config["token"]."/sendVideo?";
    $post_fields = array(
        'chat_id' => $input["message"]["chat"]["id"],
        'video' => new CURLFile(realpath("$img"))
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type:multipart/form-data"
    ));
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    $output = curl_exec($ch);
}
